import sys
import warnings
import serial
import datetime
from argparse import ArgumentParser
import sqlite3
from threading import Timer

from pykeyboard import PyKeyboard

class Dino:
    def __init__(self, keyboard, conn=None):
        self.keyboard = keyboard
        if conn:
            self._cursor = conn.cursor()
        else:
            self._cursor = None

        self._deltas = []
        self._delta_len = 15
        self._prev_value = 0.0
        self.threshhold = 10000.0
        self.in_air = False
        self._minimum_jump = False

        self._air_timer = None

    def jump(self):
        self.in_air = True
        if self._air_timer:
            self._air_timer.cancel()
        self._air_timer = Timer(0.1, self._reset_minimum_jump)
        self._air_timer.start()
        self._minimum_jump = True

        if self._cursor:
            now = datetime.datetime.now()
            self._cursor.execute("""INSERT INTO jumps VALUES (?)""", (now,))

        self.keyboard.press_key(self.keyboard.space)
        print("jump")

    def _reset_minimum_jump(self):
        self._minimum_jump = False

    def land(self):
        if self._minimum_jump:
            return
        self.in_air = False
        self.keyboard.release_key(self.keyboard.space)
        print("land")

    def duck(self):
        self._ducked = True
        self.keyboard.press_key(self.keyboard.down_key)

    def unduck(self):
        self._ducked = False
        self.keyboard.release_key(self.keyboard.down_key)

    def add_measurement(self, value):
        delta = self._prev_value - value
        self._prev_value = value

        if len(self._deltas) > self._delta_len:
            self._deltas.pop(0)
        self._deltas.append(delta)

        # print(value)
        self._process_input(value)

    def _process_input(self, value):
        jump_start = sum(self._deltas[-5:-1]) >= +80000.0
        if (value < self.threshhold and not self.in_air) or (jump_start and not self.in_air):
            # jump!
            self.jump()
        elif value > self.threshhold and self.in_air:
            self.land()

def init_db(conn):
    c = conn.cursor()
    c.execute("""CREATE TABLE jumps
                 (time timestamp)""")
    conn.commit()

def loop(ser, dino):
    while True:
        val = ser.readline().decode()
        val = val.strip()
        try:
            f = float(val)
        except ValueError:
            # print(val)
            warnings.warn("ValueError when converting to float")
            continue

        dino.add_measurement(f)

def main():
    parser = ArgumentParser()
    parser.add_argument("serial_port",
                        help="path/name of serial port. Defaults to /dev/ttyUSB0",
                        nargs="?", default="/dev/ttyUSB0")
    parser.add_argument("--db",
                        help="""write jumps with timestamp to sqlite database.
                        Use --init-db first to create file and tables""",
                        action="store_true")
    parser.add_argument("--init-db",
                        help="Create new database file and tables. Implies --db", action="store_true")
    args = parser.parse_args()

    if args.init_db:
        # also enable database when initializing
        args.db = True

    conn = None
    if args.db:
        conn = sqlite3.connect("jumps.db")

    if args.init_db:
        init_db(conn)

    ser = serial.Serial(args.serial_port, 115200, timeout=1)
    keyboard = PyKeyboard()

    dino = Dino(keyboard, conn)

    try:
        loop(ser, dino)
    finally:
        if conn:
            print("Writing out jump database, hang on...")
            conn.commit()

if __name__ == "__main__":
    main()


