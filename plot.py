import sys
import warnings

import serial
import pygame

window_size = (1920,1080)
pygame.init()
display = pygame.display.set_mode(window_size)
pygame.display.set_caption('scale plot')
font_big = pygame.font.SysFont("freesans", 400)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)
white = (255,255,255)

# Switch on 10kg
threshhold = 10000

try:
    port = sys.argv[1]
except IndexError:
    port = "/dev/ttyUSB0"

ser = serial.Serial(port, 115200, timeout=1)

space = False

while True:
    val = ser.readline().decode()
    val = val.strip()
    try:
        f = float(val)
    except ValueError:
        print(val)
        warnings.warn("ValueError when converting to float")
        continue

    print(f)
    if f < threshhold and not space:
        # jump!
        space = True
    elif f > threshhold and space:
        # touchdown!
        space = False

    if space:
        display.fill(red)
    else:
        display.fill(green)

    value_text = font_big.render(str(f), True, white)
    display.blit(value_text,(window_size[0] - value_text.get_width(), window_size[1]//2 - value_text.get_height() // 2))
    pygame.display.update()
